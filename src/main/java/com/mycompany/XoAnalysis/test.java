package com.mycompany.XoAnalysis;

import java.util.Scanner;

public class test {

    static int position;
    static int isDraw = 1;
    static boolean isWin = false;
    static boolean playGame = true;
    static String[] table = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
    static String currentPlayer = "X";
    static String playagain = "";

    public static void printWelcome() {
        System.out.println("Welcome to XO Game !!!");
    }

    public static void printTable() {
        System.out.println("");
        System.out.println(table[0] + " " + table[1] + " " + table[2] + " ");
        System.out.println(table[3] + " " + table[4] + " " + table[5] + " ");
        System.out.println(table[6] + " " + table[7] + " " + table[8] + " ");
    }

    public static void inputPosition() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please input number :");
            position = kb.nextInt();
            if (table[position - 1] == "1" || table[position - 1] == "2" || table[position - 1] == "3"
                    || table[position - 1] == "4" || table[position - 1] == "5" || table[position - 1] == "6"
                    || table[position - 1] == "7" || table[position - 1] == "8" || table[position - 1] == "9") {
                table[position - 1] = currentPlayer;
                return;
            }
        }

    }

    public static void printTurn() {
        System.out.println(currentPlayer + " Turn ");
    }

    public static void swichPlayer() {
        if (currentPlayer == "X") {
            currentPlayer = "O";
        } else {
            currentPlayer = "X";
        }
    }

    public static void checkWin() {
        if (table[0] == "X" && table[1] == "X" && table[2] == "X") {
            isWin = true;
        } else if (table[0] == "O" && table[1] == "O" && table[2] == "O") {
            isWin = true;
        } else if (table[0] == "X" && table[3] == "X" && table[6] == "X") {
            isWin = true;
        } else if (table[0] == "O" && table[3] == "O" && table[6] == "O") {
            isWin = true;
        } else if (table[1] == "X" && table[4] == "X" && table[7] == "X") {
            isWin = true;
        } else if (table[1] == "O" && table[4] == "O" && table[7] == "O") {
            isWin = true;
        } else if (table[2] == "X" && table[5] == "X" && table[8] == "X") {
            isWin = true;
        } else if (table[2] == "O" && table[5] == "O" && table[8] == "O") {
            isWin = true;
        } else if (table[3] == "X" && table[4] == "X" && table[5] == "X") {
            isWin = true;
        } else if (table[3] == "O" && table[4] == "O" && table[5] == "O") {
            isWin = true;
        } else if (table[6] == "X" && table[7] == "X" && table[8] == "X") {
            isWin = true;
        } else if (table[6] == "O" && table[7] == "O" && table[8] == "O") {
            isWin = true;
        } else if (table[0] == "X" && table[4] == "X" && table[8] == "X") {
            isWin = true;
        } else if (table[0] == "O" && table[4] == "O" && table[8] == "O") {
            isWin = true;
        } else if (table[2] == "X" && table[4] == "X" && table[6] == "X") {
            isWin = true;
        } else if (table[2] == "O" && table[4] == "O" && table[6] == "O") {
            isWin = true;

        }
    }

    public static void printWin() {
        System.out.println("Congratulations " + currentPlayer + " is the winner !!!");
    }

    public static void printPlayAgain() {
        System.out.println("Continue Y/N ?");
        Scanner kb = new Scanner(System.in);
        playagain = kb.next();
        if (playagain.equals("Y") || playagain.equals("y")) {
            reSetXO();
            printTable();
        } else if (playagain.equals("N") || playagain.equals("n")) {
            playGame = false;
        }
    }

    public static void reSetXO() {
        table[0] = "1";
        table[1] = "2";
        table[2] = "3";
        table[3] = "4";
        table[4] = "5";
        table[5] = "6";
        table[6] = "7";
        table[7] = "8";
        table[8] = "9";
        isDraw = 0;
        isWin = false;

    }

    public static void setDefaultPlayer() {
        if (playagain.equals("Y") || playagain.equals("y")) {
            currentPlayer = "X";
            playagain = "";
        }

    }

    public static void main(String[] args) {
        printWelcome();
        printTable();
        while (playGame) {
            printTurn();
            inputPosition();
            printTable();
            checkWin();
            if (isWin) {
                printTable();
                printWin();
                printPlayAgain();
            } else if (isDraw == 9) {
                System.out.println("Draw!!!");
                printPlayAgain();
            }
            swichPlayer();
            setDefaultPlayer();
            isDraw++;

        }
    }
}
