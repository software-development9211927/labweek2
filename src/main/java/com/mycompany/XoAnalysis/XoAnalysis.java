/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.XoAnalysis;

import java.util.Scanner;

/**
 *
 * @author pond
 */
public class XoAnalysis {
    public static boolean hasWinner = false;
    public static boolean continuePlay = true;
    public static Scanner sc = new Scanner(System.in);
    public static String[] arr = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
    public static String currentPlayer = "x";

    public static void printWelcome() {
        System.out.println("Welcome to XO");
    }

    public static void printarr(String[] ListXO) {
        System.out.println(ListXO[0] + " " + ListXO[1] + " " + ListXO[2]);
        System.out.println(ListXO[3] + " " + ListXO[4] + " " + ListXO[5]);
        System.out.println(ListXO[6] + " " + ListXO[7] + " " + ListXO[8]);
        System.out.println("");
    }

    public static void printCurrentTurn(String currentPlayer) {
        System.out.println(currentPlayer + " Turn");
    }

    public static String switchPlayer(String currentPlayer) {
        if (currentPlayer.equals("x")) {
            return "o";
        } else if (currentPlayer.equals("o")) {
            return "x";
        }
        return currentPlayer;
    }

    public static boolean checkWinner(String[] arr) {
        // Check rows
        for (int i = 0; i < 3; i++) {
            if (arr[i].equals(arr[i + 1]) && arr[i].equals(arr[i + 2])) {
                System.out.println("Congratulations " + arr[i] + " is the winner !!!");
                hasWinner = true;
            }

        }

        // Check columns
        for (int i = 0; i < 3; i++) {
            if (arr[i].equals(arr[i + 3]) && arr[i].equals(arr[i + 6])) {
                System.out.println("Congratulations " + arr[i] + " is the winner !!!");
                hasWinner = true;
            }
        }

        // Check diagonals
        if (arr[0].equals(arr[4]) && arr[0].equals(arr[8])) {
            System.out.println("Congratulations " + arr[0] + " is the winner !!!");
            hasWinner = true;

        } else if (arr[2].equals(arr[4]) && arr[2].equals(arr[6])) {
            System.out.println("Congratulations " + arr[2] + " is the winner !!!");
            hasWinner = true;
        }

        return hasWinner;
    }

    public static boolean printContinue() {
        System.out.println("Continue? Y/N");
        String conn = sc.next();
        if (conn.equals("y")) {
            continuePlay = true;
        } else {
            continuePlay = false;
        }
        return continuePlay;

    }

    public static void resetTable() {
        arr[0] = "1";
        arr[1] = "2";
        arr[2] = "3";
        arr[3] = "4";
        arr[4] = "5";
        arr[5] = "6";
        arr[6] = "7";
        arr[7] = "8";
        arr[8] = "9";

    }

    // เช็คว่าซ้ำหรือไม่
    public static void checkDuplicate(int num) {
        String data = arr[num - 1];
        while (data.equals("x") || data.equals("o")) {
            System.out.println("Try Again");
            printarr(arr);
            System.out.print("Please input a different number: ");
            num = sc.nextInt();
            data = arr[num - 1];
        }
        arr[num - 1] = currentPlayer;
    }

    // public static String continuePlay(){

    // }

    public static void main(String[] args) {
        printWelcome();
        printCurrentTurn(currentPlayer);

        while (continuePlay == true) {
            if (continuePlay == true) {
                hasWinner = false;
                resetTable();
            }
            while (hasWinner == false) {
                printarr(arr);
                System.out.print("Please Input number: ");
                int num = sc.nextInt();

                // กำหนดช่องที่เลือก
                // arr[num - 1] = currentPlayer;
                printarr(arr);
                checkDuplicate(num);
                checkWinner(arr);
                if (hasWinner) {
                    printContinue();
                    break;
                } else {
                    currentPlayer = switchPlayer(currentPlayer);
                    printCurrentTurn(currentPlayer);
                }
            }
        }
    }
}
